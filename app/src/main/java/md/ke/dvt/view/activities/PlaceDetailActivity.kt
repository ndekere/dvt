package md.ke.dvt.view.activities

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import coil.load
import coil.transform.CircleCropTransformation
import md.ke.dvt.data.network.resource.Status
import md.ke.dvt.R
import md.ke.dvt.data.db.DvtDatabase
import md.ke.dvt.data.network.retrofit.RetrofitInstance
import md.ke.dvt.data.repository.DvtRepository
import md.ke.dvt.databinding.ActivityPlaceDetailsBinding
import md.ke.dvt.model.places.PlaceDetailsResponse
import md.ke.dvt.viewmodel.DvtViewModel
import md.ke.dvt.viewmodel.factory.DvtViewModelFactory
import retrofit2.Response

class PlaceDetailActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityPlaceDetailsBinding
    private lateinit var dvtViewModel: DvtViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPlaceDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        setSupportActionBar(binding.toolbar)

        // TODO Set status bar background to transparent
        setStatusBar()

        setupViewModel()

        setupDetails()
    }

    private fun setupDetails() {

        dvtViewModel.getPlaceDetails(getString(R.string.google_api_key)).observe(
            this,
            { directoriesNetworkResource ->

                when (directoriesNetworkResource.status) {
                    Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        directoriesNetworkResource.data?.let { placeDetails ->
                            populatePlaceDetailsData(placeDetails as Response<PlaceDetailsResponse>)
                        }
                    }
                    Status.ERROR -> {
                        binding.progressBar.visibility = View.GONE
                        Toast.makeText(this, directoriesNetworkResource.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                }
            }
        )
    }

    private fun populatePlaceDetailsData(placeDetails: Response<PlaceDetailsResponse>) {

        val name = placeDetails.body()!!.result.name
        val lat = placeDetails.body()!!.result.geometry.location.lat
        val lng = placeDetails.body()!!.result.geometry.location.lng
        val url = placeDetails.body()!!.result.url
        val utcOffset = placeDetails.body()!!.result.utc_offset

        binding.placeName.text = name
        binding.lat.text = "Lat $lat"
        binding.lng.text = "Lng $lng"
        binding.utcOffset.text = "UTC $utcOffset"

        // TODO Places Photo API is returning 403 (Query Limit Exceed)
        binding.placeImage.load(
            "https://globalcenters.columbia.edu/sites/default/files/styles/cu_crop/public/2021-09/CGC%20Tall%20Banners%20%282%29_0.png?itok=bOzjr73M"
        ) {
            transformations(
                CircleCropTransformation()
            )
            placeholder(R.drawable.no_image)
            build()
        }

        binding.viewMore.setOnClickListener {
            Toast.makeText(applicationContext, " Visiting $url", Toast.LENGTH_LONG).show()
            val intent = Intent(applicationContext, WebviewActivity::class.java)
            intent.putExtra("URL", url)
            startActivity(intent)
        }
    }

    private fun setupViewModel() {
        dvtViewModel = ViewModelProviders.of(
            this,
            DvtViewModelFactory(
                DvtRepository(
                    RetrofitInstance.retrofitInterface,
                    DvtDatabase.getInstance(applicationContext)
                )
            )
        ).get(DvtViewModel::class.java)
    }

    private fun setStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(md.ke.dvt.R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) ||
            super.onSupportNavigateUp()
    }
}
