package md.ke.dvt.view.compose

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import md.ke.dvt.DvtApplication
import md.ke.dvt.R
import md.ke.dvt.common.Utils
import md.ke.dvt.model.WeatherResponse
import md.ke.dvt.ui.theme.DvtTheme
import md.ke.dvt.viewmodel.DvtViewModel
import md.ke.dvt.viewmodel.factory.DvtViewModelFactory
import java.util.*

class FavouritedPlacesActivity : ComponentActivity() {

    private val mainViewModel: DvtViewModel by viewModels {
        DvtViewModelFactory((application as DvtApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DvtTheme {
                Favourited(list = mainViewModel.favoritedWeatherListResponse)
                mainViewModel.getFavouritePlacesToMutable()
            }
        }
    }
}

@Composable
fun Favourited(list: List<WeatherResponse>) {
    var selectedIndex by remember { mutableStateOf(-1) }
    LazyColumn {
        itemsIndexed(items = list) { index, item ->
            FavouritedItem(weather = item, index, selectedIndex) { i ->
                selectedIndex = i
            }
        }
    }
}

@Composable
fun FavouritedItem(
    weather: WeatherResponse,
    index: Int,
    selectedIndex: Int,
    onClick: (Int) -> Unit
) {

    val expanded = rememberSaveable { mutableStateOf(false) }
    val favorited = rememberSaveable { mutableStateOf(true) }
    val context = LocalContext.current

    Surface(
        color = MaterialTheme.colors.primary,
        modifier = Modifier.padding(horizontal = 4.dp, vertical = 8.dp)
    ) {
        Row(
            modifier = Modifier
                .padding(24.dp)
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )

        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(bottom = 12.dp)

            ) {
                IconButton(
                    onClick = {
                        if (favorited.value) Toast.makeText(
                            context,
                            "TODO > Remove from favourites", Toast.LENGTH_SHORT
                        ).show()
                        else Toast.makeText(context, "TODO > Add to favourites", Toast.LENGTH_SHORT).show()
                        favorited.value = !favorited.value
                    }
                ) {
                    Icon(
                        imageVector = if (favorited.value) Icons.Filled.Favorite
                        else Icons.Outlined.Favorite,
                        contentDescription =
                        if (favorited.value) stringResource(R.string.favourite)
                        else stringResource(R.string.unfavourite)
                    )
                }

                Text(
                    text = weather.name!!,
                    style = MaterialTheme.typography.h4.copy(
                        fontWeight = FontWeight.Bold
                    )
                )
                if (expanded.value) {
                    val speed = weather.wind?.speed
                    val date = Utils().getDate(weather.dt!!.toLong())
                    val sunrise = Utils().getTime(weather.sys!!.sunrise)
                    val sunset = Utils().getTime(weather.sys.sunset)

                    Column() {
                        Text(text = ("The speed of wind on $date was $speed"))
                        Text(text = ("Sunrise was at $sunrise"))
                        Text(text = ("Sunset was at $sunset"))
                    }
                }
            }

            IconButton(
                onClick = { expanded.value = !expanded.value }
            ) {
                Icon(
                    imageVector = if (expanded.value) Icons.Filled.ExpandLess
                    else Icons.Filled.ExpandMore,
                    contentDescription = if (expanded.value) stringResource(R.string.show_less)
                    else stringResource(R.string.show_more)
                )
            }
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 320,
    uiMode = UI_MODE_NIGHT_YES,
    name = "DefaultPreviewDark"
)
@Preview(showBackground = true, widthDp = 320)
@Composable
fun DefaultPreview() {
    DvtTheme {
//        Favourited(list = mainViewModel.favoritedWeatherListResponse)
    }
}
