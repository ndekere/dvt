package md.ke.dvt.view.activities

import android.R
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import md.ke.dvt.common.SharedPrefs
import md.ke.dvt.data.db.DvtDatabase
import md.ke.dvt.data.network.retrofit.RetrofitInstance
import md.ke.dvt.data.repository.DvtRepository
import md.ke.dvt.databinding.ActivityMapsBinding
import md.ke.dvt.model.WeatherResponse
import md.ke.dvt.viewmodel.DvtViewModel
import md.ke.dvt.viewmodel.factory.DvtViewModelFactory
import timber.log.Timber
import java.util.*

class MapsActivity : FragmentActivity(), OnMapReadyCallback {

    private lateinit var dvtViewModel: DvtViewModel

    private var mMap: GoogleMap? = null
    private var mapFragment: SupportMapFragment? = null
    private lateinit var binding: ActivityMapsBinding
    private lateinit var favouritePlaces: List<WeatherResponse>

    // creating array list for adding all our locations.
    private var locationArrayList: ArrayList<LatLng>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // TODO Set status bar background to transparent >> Should move to utils class
        setStatusBar()
        setupViewModel()
        setupFavoriteLocationsArrayList()
    }

    private fun setupMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager.findFragmentById(md.ke.dvt.R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun setupFavoriteLocationsArrayList() {
        // in below line we are initializing our array list.
        locationArrayList = ArrayList<LatLng>()

        Timber.e("Fetching Favorite Locations")

        dvtViewModel.getFavouritePlaces().observe(
            this,
            Observer {
                val favouritePlaces = it

                Timber.tag("Location favouritePlaces").e(it.toString())

                if (favouritePlaces.isNotEmpty()) Timber.e("RES : %s", favouritePlaces[0].toString())

                // Here we add our locations in our array list.
                for (item in favouritePlaces) {
                    Timber.tag("Location Items").e(item.toString())

                    locationArrayList!!.add(LatLng(item.coord!!.lat, item.coord.lon))

                    Timber.tag("locationArrayList").e(locationArrayList.toString())
                }

                if (locationArrayList!!.isNotEmpty()) setupMap()
            }
        )
    }

    private fun setupViewModel() {
        dvtViewModel = ViewModelProviders.of(
            this,
            DvtViewModelFactory(
                DvtRepository(
                    RetrofitInstance.retrofitInterface,
                    DvtDatabase.getInstance(applicationContext)
                )
            )
        ).get(DvtViewModel::class.java)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap

        val currentLocationLat: Double = SharedPrefs(this).getLat()!!.toDouble()
        val currentLocationLon: Double = SharedPrefs(this).getLon()!!.toDouble()

        val currentLatLon = LatLng(currentLocationLat, currentLocationLon)

        mMap!!.addMarker(
            MarkerOptions()
                .position(currentLatLon)
                .title("My Location")
//                .icon(BitmapDescriptorFactory.fromResource(md.ke.dvt.R.drawable.ic_baseline_my_location_24))
        )

        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLon, 19F))

        for (i in locationArrayList!!.indices) {
            // Add marker to each location of our array list.
            mMap!!.addMarker(MarkerOptions().position(locationArrayList!![i]).title("Marker"))
        }
    }

    private fun setStatusBar() {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}
