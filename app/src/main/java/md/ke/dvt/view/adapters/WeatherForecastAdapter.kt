package md.ke.dvt.view.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import md.ke.dvt.R
import md.ke.dvt.common.Utils
import md.ke.dvt.databinding.FragmentWeatherForecastItemBinding
import md.ke.dvt.model.WeatherForecast
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class WeatherForecastAdapter(private val weatherForecast: ArrayList<WeatherForecast>) :
    RecyclerView.Adapter<WeatherForecastAdapter.WeatherForecastViewHolder>() {

    class WeatherForecastViewHolder(private val binding: FragmentWeatherForecastItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(weatherForecast: WeatherForecast) {
            itemView.apply {
                binding.day.text = Utils().getDayFromEpochTime(weatherForecast.dt)

                val icon = getIcon(binding.root.context, weatherForecast.weather[0].main)
                binding.icon.setImageDrawable(icon)

                binding.temperature.text = String.format(
                    context.getString(R.string.currentTempBig),
                    Utils().convertKelvinToCelsius(weatherForecast.main.temp).toString()
                )
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        private fun getIcon(context: Context, name: String): Drawable? {
            return when {
                name.lowercase(Locale.getDefault()).contains("rain") -> {
                    context.resources.getDrawable(R.drawable.rain_3x)
                }
                name.lowercase(Locale.getDefault()).contains("sunny") -> {
                    context.resources.getDrawable(R.drawable.clear_3x)
                }
                name.lowercase(Locale.getDefault()).contains("cloudy") -> {
                    context.resources.getDrawable(R.drawable.partlysunny_3x)
                }
                else -> {
                    // for unhandled weather groups > I return sunny :(
                    context.resources.getDrawable(R.drawable.clear_3x)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
        WeatherForecastViewHolder {
        val binding = FragmentWeatherForecastItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WeatherForecastViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WeatherForecastViewHolder, position: Int) {
        holder.bind(weatherForecast[position])
    }

    override fun getItemCount() = weatherForecast.size

    fun addData(weatherForecast: ArrayList<WeatherForecast>?) {
        this.weatherForecast.apply {
            clear()
            // Create a new array with one forecast per day
            // Instead of every 3 hours > 24/3
            val res = getSingleForecastPerDay(weatherForecast)
            addAll(res)
        }
    }

    private fun getSingleForecastPerDay(weatherForecast: ArrayList<WeatherForecast>?): ArrayList<WeatherForecast> {
        val res = arrayListOf<WeatherForecast>()
        var i = 0
        while (i < weatherForecast!!.size) {
            Timber.tag("DAY WEATHER: ").e(weatherForecast[i].dt_txt)
            res.add(weatherForecast[i])
            i += 8
        }
        return res
    }
}
