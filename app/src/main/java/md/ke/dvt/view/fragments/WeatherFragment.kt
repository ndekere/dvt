package md.ke.dvt.view.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import kotlinx.coroutines.launch
import md.ke.dvt.R
import md.ke.dvt.common.SharedPrefs
import md.ke.dvt.common.Utils
import md.ke.dvt.data.db.DvtDatabase
import md.ke.dvt.data.network.resource.Status
import md.ke.dvt.data.network.retrofit.RetrofitInstance
import md.ke.dvt.data.repository.DvtRepository
import md.ke.dvt.databinding.FragmentWeatherBinding
import md.ke.dvt.model.WeatherForecast
import md.ke.dvt.model.WeatherForecastResponse
import md.ke.dvt.model.WeatherResponse
import md.ke.dvt.view.activities.MapsActivity
import md.ke.dvt.view.activities.PlaceDetailActivity
import md.ke.dvt.view.adapters.WeatherForecastAdapter
import md.ke.dvt.view.compose.FavouritedPlacesActivity
import md.ke.dvt.viewmodel.DvtViewModel
import md.ke.dvt.viewmodel.factory.DvtViewModelFactory
import timber.log.Timber
import java.util.*

class WeatherFragment : Fragment() {

    private lateinit var dvtViewModel: DvtViewModel
    private lateinit var adapter: WeatherForecastAdapter

    private var latitude: String? = null
    private var longitude: String? = null

    private val PERMISSIONID = 42
    private val REQUEST_LOCATION = 43

    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var _binding: FragmentWeatherBinding? = null
    private var weatherResponse: WeatherResponse? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewModel()
        setupRecyclerView()
        setupWeatherInfo()
    }

    private fun setupWeatherInfo() {

        if (checkConnection()) {

            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
            getLastLocation()
            if (latitude == null || longitude == null) {
                Toast.makeText(
                    context, "Detecting your location. Please wait!",
                    Toast.LENGTH_SHORT
                ).show()
                return
            } else {
                fetchWeatherDataOnline()
            }
        } else {
            Toast.makeText(
                context, "Not connected!",
                Toast.LENGTH_SHORT
            ).show()

            // TODO Load last location from DB
            fetchWeatherDataLocally()
        }
    }

    private fun checkConnection(): Boolean {
        val cm =
            requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo

        return activeNetwork?.isConnected == true
    }

    private fun fetchWeatherDataOnline() {

        dvtViewModel.getCurrentWeather(latitude!!, longitude!!).observe(
            viewLifecycleOwner,
            Observer
            { directoriesNetworkResource ->

                when (directoriesNetworkResource.status) {
                    Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        binding.recyclerView.visibility = View.VISIBLE
                        binding.info.visibility = View.VISIBLE
                        binding.divider.visibility = View.VISIBLE

                        directoriesNetworkResource.data?.let {
                            Toast.makeText(
                                context,
                                directoriesNetworkResource.message,
                                Toast.LENGTH_LONG
                            ).show()
                            Timber.e(it.body().toString())

                            lifecycleScope.launch {
                                dvtViewModel.addCurrentWeatherToRoom(it.body()!!)
                            }

                            setupCurrentWeather(it.body()!!)
                            setBackgroundImageAccordingly(it.body()!!)
                        }
                    }
                    Status.ERROR -> {
                        binding.recyclerView.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        Toast.makeText(context, directoriesNetworkResource.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                        binding.recyclerView.visibility = View.GONE
                    }
                }
            }
        )

        dvtViewModel.getWeatherForecast(latitude!!, longitude!!).observe(
            viewLifecycleOwner,
            Observer {
                Timber.tag("FORECAST WEATHER :").e(it.body().toString())
                setupWeatherForecast(it.body())
            }
        )
    }

    private fun fetchWeatherDataLocally() {

        dvtViewModel.getCurrentWeatherLocally().observe(
            viewLifecycleOwner,
            Observer {
                setupCurrentWeather(it)
                setBackgroundImageAccordingly(it)
            }
        )

        dvtViewModel.getWeatherForecastLocally().observe(
            viewLifecycleOwner,
            Observer {
                setupWeatherForecast(it)
            }
        )
    }

    private fun setupWeatherForecast(it: WeatherForecastResponse?) {
        populateWeatherForecastData(it!!.weatherForecast)
    }

    private fun populateWeatherForecastData(weatherForecast: ArrayList<WeatherForecast>?) {
        adapter.apply {
            addData(weatherForecast)
            notifyDataSetChanged()
        }
    }

    private fun setupRecyclerView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = WeatherForecastAdapter(arrayListOf())
        // TODO Divider not necessary, you can uncomment this line to have a look though
        // binding.recyclerView.addItemDecoration(DividerItemDecoration(binding.recyclerView.context,
        // (binding.recyclerView.layoutManager as LinearLayoutManager).orientation))
        binding.recyclerView.adapter = adapter
    }

    private fun setBackgroundImageAccordingly(it: WeatherResponse) {

        // weather.main Group of weather parameters (Rain, Snow, Extreme etc.)

        when {
            it.weather?.get(0)?.main?.lowercase(Locale.getDefault())?.contains("rain")!! -> {
                binding.image.setImageResource(R.drawable.forest_rainy)
                binding.parent.setBackgroundColor(resources.getColor(R.color.rainy))
            }
            it.weather[0].main.lowercase(Locale.getDefault()).contains("sunny") -> {
                binding.image.setImageResource(R.drawable.forest_sunny)
                binding.parent.setBackgroundColor(resources.getColor(R.color.sunny))
            }
            it.weather[0].main.lowercase(Locale.getDefault()).contains("cloudy") -> {
                binding.image.setImageResource(R.drawable.forest_cloudy)
                binding.parent.setBackgroundColor(resources.getColor(R.color.cloudy))
            }
            else -> {
                // for unhandled weather groups > I return sunny :(
                binding.image.setImageResource(R.drawable.forest_sunny)
            }
        }
    }

    private fun setupCurrentWeather(it: WeatherResponse) {

        weatherResponse = it

        Timber.tag("CURRENT WEATHER").e(weatherResponse.toString())

        setClickListeners()

        binding.currentTemp.text = String.format(
            getString(R.string.currentTempBig),
            Utils().convertKelvinToCelsius(it.main!!.temp).toString()
        )

        binding.currentCloud.text = it.weather!![0].main

        binding.current.text = String.format(
            getString(R.string.currentTemp),
            Utils().convertKelvinToCelsius(it.main.temp).toString()
        )
        binding.maximum.text = String.format(
            getString(R.string.maximumTemp),
            Utils().convertKelvinToCelsius(it.main.temp_max).toString()
        )
        binding.minimum.text = String.format(
            getString(R.string.minimumTemp),
            Utils().convertKelvinToCelsius(it.main.temp_min).toString()
        )
    }

    private fun setupViewModel() {
        dvtViewModel = ViewModelProviders.of(
            this,
            DvtViewModelFactory(
                DvtRepository(
                    RetrofitInstance.retrofitInterface,
                    DvtDatabase.getInstance(requireContext())
                )
            )
        ).get(DvtViewModel::class.java)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (Utils().isLocationEnabled(requireContext())) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(requireActivity()) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {

                        latitude = location.latitude.toString()
                        longitude = location.longitude.toString()

                        SharedPrefs(requireContext()).setLat(latitude!!)
                        SharedPrefs(requireContext()).setLon(longitude!!)

                        fetchWeatherDataOnline()
                    }
                }
            } else {
                Toast.makeText(
                    requireContext(),
                    "Please enable locations to give you weather forecast",
                    Toast.LENGTH_SHORT
                ).show()
                // TODO This should be called on dialog okay pressed > not auto
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivityForResult(intent, REQUEST_LOCATION)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation

            latitude = mLastLocation.latitude.toString()
            longitude = mLastLocation.longitude.toString()

            SharedPrefs(requireContext()).setLat(latitude!!)
            SharedPrefs(requireContext()).setLon(longitude!!)

            fetchWeatherDataOnline()

            Timber.tag("LOCATION****: ").e(mLastLocation.toString())
        }
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSIONID
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(requireContext(), "Permission Granted", Toast.LENGTH_SHORT).show()
                setupWeatherInfo()
            } else {
                Toast.makeText(requireContext(), "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_LOCATION) {
            Toast.makeText(requireContext(), "Location Result Callback", Toast.LENGTH_SHORT).show()
            setupWeatherInfo()
        }
    }

    private fun setClickListeners() {
        binding.info.bringToFront()

        val popupMenu = PopupMenu(requireContext(), binding.info)
        popupMenu.menuInflater.inflate(R.menu.menu_main, popupMenu.menu)

        val placeName = weatherResponse?.name

        Timber.e(weatherResponse.toString())

        val menuOpts: Menu = popupMenu.menu
        menuOpts.getItem(0).title = "Favourite $placeName"
        menuOpts.getItem(2).title = "More about $placeName"

        dvtViewModel.getFavouritePlaces().observe(
            viewLifecycleOwner,
            Observer {
                if (it.isEmpty()) {
                    menuOpts.getItem(1).isVisible = false
                    menuOpts.getItem(2).isVisible = false
                    menuOpts.getItem(3).isVisible = false
                }
            }
        )

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.favourite -> favouritePlace()
                R.id.view_favourite -> viewFavouritePlaces()
                R.id.more_information -> moreInformation()
                R.id.view_on_map -> viewOnMap()
            }
            true
        }

        binding.info.setOnClickListener {
            popupMenu.show()
        }
    }

    private fun viewOnMap() {
        val intent = Intent(context, MapsActivity::class.java)
        startActivity(intent)
    }

    private fun moreInformation() {
        val intent = Intent(context, PlaceDetailActivity::class.java)
        startActivity(intent)
    }

    private fun viewFavouritePlaces() {
        val intent = Intent(context, FavouritedPlacesActivity::class.java)
        startActivity(intent)
    }

    private fun favouritePlace() {
        dvtViewModel.updatePlace().observe(
            viewLifecycleOwner,
            {
                // On update
                Toast.makeText(
                    requireContext(),
                    "Added to favorites", Toast.LENGTH_SHORT
                ).show()
                // Refresh listeners
                setClickListeners()
            }
        )
    }
}
