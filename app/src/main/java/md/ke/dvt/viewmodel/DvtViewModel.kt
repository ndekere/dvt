package md.ke.dvt.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import md.ke.dvt.data.network.resource.DirectoriesNetworkResource
import md.ke.dvt.data.network.retrofit.RetrofitInstance
import md.ke.dvt.data.network.retrofit.RetrofitMapsInstance
import md.ke.dvt.data.repository.DvtRepository
import md.ke.dvt.data.repository.MapsRepository
import md.ke.dvt.model.WeatherResponse
import md.ke.dvt.model.places.PlaceDetailsResponse
import md.ke.dvt.model.places.PlacePhotoResponse
import md.ke.dvt.model.places.PlacesNearbyResponse
import retrofit2.Response
import timber.log.Timber
import kotlin.Exception as Exception1

class DvtViewModel(
    private val repository: DvtRepository
) : ViewModel() {

    var favoritedWeatherListResponse: List<WeatherResponse> by mutableStateOf(listOf())
    private var errorMessage: String by mutableStateOf("")

    private val mapsRepository: MapsRepository = MapsRepository(RetrofitMapsInstance.retrofitInterface)

    // TODO Using Room + Flow Using LiveData and caching what is returned has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.

    fun getCurrentWeather(lat: String, lon: String) = liveData(Dispatchers.IO) {
        emit(DirectoriesNetworkResource.loading(data = null))
        try {
            emit(
                DirectoriesNetworkResource.success(
                    data = repository.fetchCurrentWeather(lat, lon, RetrofitInstance.appId)
                )
            )
        } catch (exception: Exception1) {
            Timber.e(exception.message.toString())
            emit(
                DirectoriesNetworkResource.error(
                    message = exception.message ?: "Error Occurred"
                )
            )
        }
    }

    suspend fun addCurrentWeatherToRoom(weatherResponse: WeatherResponse) =
        repository.addCurrentWeatherToRoom(weatherResponse)

    fun getCurrentWeatherLocally() = liveData {
        val res = repository.fetchCurrentWeatherFromRoom()
        emit(res)
    }

    fun getPlaceDetails(key: String) = liveData(Dispatchers.IO) {
        emit(DirectoriesNetworkResource.loading(data = null))
        val res = repository.fetchCurrentWeatherFromRoom()

        try {
            val place = searchPlace(res.name, key)
            if (place.body() != null) {
                val placeId: String = place.body()!!.results[0].place_id
                val placeDetails: Response<PlaceDetailsResponse> = placeDetails(placeId, key)
                emit(DirectoriesNetworkResource.success(data = placeDetails))

                if (placeDetails.body() != null) {
                    // TODO Get a photo of the location >> Throws a 403 >> Query Limit Exceeded :(
                    val placePhotoRef: String = placeDetails.body()!!.result.photos[0].photo_reference
                    val placePhoto = placePhoto(placePhotoRef, key)
                } else {
                    emit(DirectoriesNetworkResource.error<String>("Place Details Error"))
                }
            } else {
                emit(DirectoriesNetworkResource.error<String>("Place Error"))
            }
        } catch (exception: Exception1) {
            Timber.e(exception.message.toString())
            emit(
                DirectoriesNetworkResource.error(
                    message = exception.message ?: "Error Occurred"
                )
            )
        }
    }

    fun getWeatherForecast(lat: String, lon: String) = liveData {
        emit(repository.fetchWeatherForecast(lat, lon, RetrofitInstance.appId))
    }

    fun getWeatherForecastLocally() = liveData {
        emit(repository.fetchWeatherForecastFromRoom())
    }

    fun updatePlace() = liveData {
        emit(repository.updatePlace())
    }

    fun getFavouritePlaces() = liveData {
        emit(repository.getFavouritePlaces())
    }

    fun getFavouritePlacesToMutable() {
        viewModelScope.launch {
            try {
                val weatherResponse = repository.getFavouritePlaces()
                favoritedWeatherListResponse = weatherResponse

                Timber.e(favoritedWeatherListResponse.toString())
            } catch (e: Exception1) {
                errorMessage = e.message.toString()
            }
        }
    }

    suspend fun searchPlace(name: String?, key: String): Response<PlacesNearbyResponse> {
        return mapsRepository.searchPlace(name, key)
    }

    suspend fun placeDetails(placeId: String, key: String): Response<PlaceDetailsResponse> {
        return mapsRepository.placeDetails(placeId, key)
    }

    suspend fun placePhoto(placePhotoRef: String, key: String): Response<PlacePhotoResponse> {
        return mapsRepository.placePhoto(placePhotoRef, key)
    }

//    suspend fun getNearbyPlaces(latLng: LatLng, i: String, s: String, s1: String, googleApiKey: String) {
//
//        mapsRepository.getNearbyPlaces(latLng, i, s, s1, googleApiKey)
//    }
//
//    suspend fun favouritePlace(weatherResponse: WeatherResponse) {
//        repository.favouritePlace(weatherResponse)
//    }
}
