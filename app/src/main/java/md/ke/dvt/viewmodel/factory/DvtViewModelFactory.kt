package md.ke.dvt.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import md.ke.dvt.data.repository.DvtRepository
import md.ke.dvt.viewmodel.DvtViewModel

class DvtViewModelFactory(private val repository: DvtRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DvtViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DvtViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
