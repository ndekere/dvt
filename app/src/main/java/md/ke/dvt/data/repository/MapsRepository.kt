package md.ke.dvt.data.repository

import com.google.android.gms.maps.model.LatLng
import md.ke.dvt.data.network.retrofit.RetrofitInterface
import md.ke.dvt.model.places.PlaceDetailsResponse
import md.ke.dvt.model.places.PlacePhotoResponse
import md.ke.dvt.model.places.PlacesNearbyResponse
import retrofit2.Response
import timber.log.Timber

class MapsRepository(
    private val retrofitInterface: RetrofitInterface
) {

    suspend fun getNearbyPlaces(latLng: LatLng, i: String, s: String, s1: String, googleApiKey: String) {

        val ll: String = latLng.latitude.toString() + "," + latLng.longitude.apply { }

        val place = retrofitInterface.getNearbyPlaces(
            ll, i, s, s1, googleApiKey
        )

        Timber.e(place.body().toString())
    }

    suspend fun searchPlace(name: String?, key: String): Response<PlacesNearbyResponse> {
        return retrofitInterface.searchPlace(name!!, key)
    }

    suspend fun placeDetails(placeId: String, key: String): Response<PlaceDetailsResponse> {
        return retrofitInterface.placeDetails(placeId, key)
    }

    suspend fun placePhoto(placePhotoRef: String, key: String): Response<PlacePhotoResponse> {

        return retrofitInterface.placePhoto(placePhotoRef, key)
    }
}
