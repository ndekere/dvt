package md.ke.dvt.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import md.ke.dvt.model.WeatherResponse
import md.ke.dvt.model.ext.Wind

class FavouriteConverter {

    @TypeConverter
    fun toString(weatherResponse: WeatherResponse): String = Gson().toJson(weatherResponse)

    @TypeConverter
    fun fromString(string: String): WeatherResponse = Gson().fromJson(string, WeatherResponse::class.java)


}