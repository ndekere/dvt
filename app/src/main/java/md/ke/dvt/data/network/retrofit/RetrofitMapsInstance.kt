package md.ke.dvt.data.network.retrofit

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitMapsInstance {

    private const val MAIN_URL = "https://maps.googleapis.com/"

    var gson: Gson? = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        .create()

    private val client = buildOkhttpClient()

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(MAIN_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    val retrofitInterface: RetrofitInterface = getRetrofit().create(RetrofitInterface::class.java)
}
