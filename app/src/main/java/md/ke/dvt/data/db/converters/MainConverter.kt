package md.ke.dvt.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import md.ke.dvt.model.ext.Main

class MainConverter {

    @TypeConverter
    fun toString(main: Main): String = Gson().toJson(main)

    @TypeConverter
    fun fromString(string: String): Main = Gson().fromJson(string, Main::class.java)


}