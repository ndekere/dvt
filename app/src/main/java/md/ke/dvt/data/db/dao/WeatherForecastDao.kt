package md.ke.dvt.data.db.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import md.ke.dvt.model.WeatherForecastResponse
import md.ke.dvt.model.WeatherResponse

@Dao
interface WeatherForecastDao {

    @Query("SELECT * FROM WeatherForecastResponse")
    fun fetchWeatherForecast(): Flow<List<WeatherForecastResponse>>

    @Query("SELECT * FROM WeatherForecastResponse ORDER BY local_id DESC LIMIT 1 ")
    suspend fun fetchWeatherForecastLatest(): WeatherForecastResponse

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: WeatherForecastResponse)

    @Delete
    suspend fun delete(item: WeatherForecastResponse)
}
