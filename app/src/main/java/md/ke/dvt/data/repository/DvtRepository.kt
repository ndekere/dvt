package md.ke.dvt.data.repository

import com.google.android.gms.maps.model.LatLng
import md.ke.dvt.data.db.DvtDatabase
import md.ke.dvt.data.network.retrofit.RetrofitInterface
import md.ke.dvt.model.WeatherForecastResponse
import md.ke.dvt.model.WeatherResponse
import retrofit2.Response
import timber.log.Timber

class DvtRepository(
    private val retrofitInterface: RetrofitInterface,
    private val dvtDatabase: DvtDatabase
) {

    // TODO Fetch from NETWORK via RETROFIT and then save to DB which is being observed
    suspend fun fetchCurrentWeather(
        lat: String,
        lon: String,
        appId: String
    ): Response<WeatherResponse> {
        return retrofitInterface.fetchCurrentWeather(lat, lon, appId)
    }

    suspend fun fetchCurrentWeatherFromRoom(): WeatherResponse {
        return dvtDatabase.weatherDao().fetchCurrentWeatherLatest()
    }

    suspend fun fetchWeatherForecast(
        lat: String,
        lon: String,
        appId: String
    ): Response<WeatherForecastResponse> {
        val weatherForecast = retrofitInterface.fetchWeatherForecast(lat, lon, appId)
        // Save to DB
        dvtDatabase.weatherForecastDao().insert(weatherForecast.body()!!)
        return weatherForecast
    }

    suspend fun fetchWeatherForecastFromRoom(): WeatherForecastResponse {
        return dvtDatabase.weatherForecastDao().fetchWeatherForecastLatest()
    }

    suspend fun updatePlace() {
        val weatherResponse = fetchCurrentWeatherLatest()
        dvtDatabase.weatherDao().update(1, weatherResponse.local_id!!)
    }

    private suspend fun fetchCurrentWeatherLatest(): WeatherResponse {
        return dvtDatabase.weatherDao().fetchCurrentWeatherLatest()
    }

    suspend fun getFavouritePlaces(): List<WeatherResponse> {
        return dvtDatabase.weatherDao().getFavouritedPlaces()
    }

    suspend fun addCurrentWeatherToRoom(weatherResponse: WeatherResponse) {
        dvtDatabase.weatherDao().insert(weatherResponse)
    }

    suspend fun getPlace(latLng: LatLng, i: String, s: String, s1: String, googleApiKey: String) {

        val ll: String = latLng.latitude.toString() + "," + latLng.longitude.apply { }

        val place = retrofitInterface.getNearbyPlaces(
            ll, i, s, s1, googleApiKey
        )

        Timber.e(place.toString())
    }

    suspend fun favouritePlace(weatherResponse: WeatherResponse) {
        Timber.e(weatherResponse.toString())

        dvtDatabase.favouriteDao().insert(weatherResponse)
    }
}
