package md.ke.dvt.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import md.ke.dvt.model.ext.City
import md.ke.dvt.model.ext.Clouds

class CityConverter {
    @TypeConverter
    fun toString(city: City): String = Gson().toJson(city)
    @TypeConverter
    fun fromString(string: String): City = Gson().fromJson(string, City::class.java)
}
