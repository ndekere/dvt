package md.ke.dvt.data.network.resource

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
