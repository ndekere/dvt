package md.ke.dvt.data.network.resource

data class DirectoriesNetworkResource<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): DirectoriesNetworkResource<T> =
            DirectoriesNetworkResource(
                status = Status.SUCCESS,
                data = data,
                message = "Success"
            )

        fun <T> error(message: String): DirectoriesNetworkResource<T> =
            DirectoriesNetworkResource(
                status = Status.ERROR,
                data = null,
                message = message
            )

        fun <T> loading(data: T): DirectoriesNetworkResource<T> =
            DirectoriesNetworkResource(
                status = Status.LOADING,
                data = data,
                message = null
            )
    }
}
