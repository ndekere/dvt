package md.ke.dvt.data.db.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import md.ke.dvt.model.WeatherResponse

@Dao
interface FavouriteDao {

    @Query("SELECT * FROM favourite")
    fun fetchFavouritedPlace(): List<WeatherResponse>

    @Insert
    suspend fun insert(item: WeatherResponse)

    @Delete
    suspend fun delete(item: WeatherResponse)

}
