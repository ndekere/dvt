package md.ke.dvt.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import md.ke.dvt.data.db.converters.*
import md.ke.dvt.data.db.dao.FavouriteDao
import md.ke.dvt.data.db.dao.WeatherDao
import md.ke.dvt.data.db.dao.WeatherForecastDao
import md.ke.dvt.model.Favourite
import md.ke.dvt.model.WeatherForecastResponse
import md.ke.dvt.model.WeatherResponse

@Database(
    version = 3,
    entities = [
        WeatherResponse::class,
        WeatherForecastResponse::class,
        Favourite::class
    ],
    exportSchema = true
)
@TypeConverters(
    CoordinatesConverter::class,
    WeatherConverter::class,
    MainConverter::class,
    WindConverter::class,
    CloudsConverter::class,
    SysConverter::class,
    WeatherForecastConverter::class,
    CityConverter::class,
    FavouriteConverter::class
)
abstract class DvtDatabase : RoomDatabase() {

    abstract fun weatherDao(): WeatherDao
    abstract fun weatherForecastDao(): WeatherForecastDao
    abstract fun favouriteDao(): FavouriteDao

    companion object {

        private var INSTANCE: DvtDatabase? = null

        @Synchronized
        fun getInstance(
            context: Context
        ): DvtDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    DvtDatabase::class.java,
                    "dvt_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return INSTANCE!!
        }
    }
}
