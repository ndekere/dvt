package md.ke.dvt.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import md.ke.dvt.model.WeatherForecast
import md.ke.dvt.model.ext.Weather
import java.lang.reflect.Type

class WeatherForecastConverter {

    @TypeConverter
    fun toString(weatherForecast: ArrayList<WeatherForecast?>?): String {
        val gson = Gson()
        return gson.toJson(weatherForecast)
    }

    @TypeConverter
    fun fromString(string: String?): ArrayList<WeatherForecast> {
        val listType: Type = object : TypeToken<ArrayList<WeatherForecast?>?>() {}.type
        return Gson().fromJson(string, listType)
    }

}