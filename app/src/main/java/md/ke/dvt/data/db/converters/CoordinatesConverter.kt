package md.ke.dvt.data.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import md.ke.dvt.model.ext.Coord

class CoordinatesConverter {

    @TypeConverter
    fun toString(coord: Coord): String = Gson().toJson(coord)

    @TypeConverter
    fun fromString(string: String): Coord = Gson().fromJson(string, Coord::class.java)


}