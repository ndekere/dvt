package md.ke.dvt.data.db.dao

import androidx.room.*
import kotlinx.coroutines.flow.Flow
import md.ke.dvt.model.WeatherResponse

@Dao
interface WeatherDao {

    @Query("SELECT * FROM WeatherResponse")
    fun fetchCurrentWeather(): Flow<List<WeatherResponse>>

    @Query("SELECT * FROM WeatherResponse ORDER BY local_id DESC LIMIT 1 ")
    suspend fun fetchCurrentWeatherLatest(): WeatherResponse

    // Todo We'll use this to update
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: WeatherResponse)

    @Delete
    suspend fun delete(item: WeatherResponse)

    @Query("UPDATE WeatherResponse SET isFavourite=:value WHERE local_id = :id")
    suspend fun update(value: Int, id: Int)

    @Query("SELECT * FROM WeatherResponse WHERE isFavourite = 1 ORDER BY local_id DESC")
    suspend fun getFavouritedPlaces(): List<WeatherResponse>
}
