package md.ke.dvt.data.network.retrofit

import androidx.databinding.ktx.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

fun buildOkhttpClient(): OkHttpClient {

    val interceptor = HttpLoggingInterceptor()
    interceptor.level = when (BuildConfig.BUILD_TYPE) {
        "release" -> HttpLoggingInterceptor.Level.NONE
        else -> HttpLoggingInterceptor.Level.BODY
    }

    // Create the Collector
//    val chuckerCollector = ChuckerCollector(
//        context = DvtApplication().getAppContext()!!,
//        // Toggles visibility of the push notification
//        showNotification = true,
//        // Allows to customize the retention period of collected data
//        retentionPeriod = RetentionManager.Period.ONE_HOUR
//    )

    // Create the Interceptor
//    val chuckerInterceptor = ChuckerInterceptor.Builder(DvtApplication().getAppContext()!!)
//        // The previously created Collector
//        .collector(chuckerCollector)
//        // The max body content length in bytes, after this responses will be truncated.
//        .maxContentLength(length = 250000L)
//        // List of headers to replace with ** in the Chucker UI
//        .redactHeaders("Auth-Token", "Bearer")
//        // Read the whole response body even when the client does not consume the response completely.
//        // This is useful in case of parsing errors or when the response body
//        // is closed before being read like in Retrofit with Void and Unit types.
//        .alwaysReadResponseBody(true)
//        // Use decoder when processing request and response bodies. When multiple decoders are installed they
//        // are applied in an order they were added.
//        // addBodyDecoder(decoder)
//        // Controls Android shortcut creation. Available in SNAPSHOTS versions only at the moment
//        // createShortcut(true)
//        .build()

    return OkHttpClient.Builder()
        .addInterceptor(interceptor)
//        .addInterceptor(chuckerInterceptor)
        .connectTimeout(
            timeout = 60,
            unit = TimeUnit.SECONDS
        )
        .readTimeout(timeout = 60, unit = TimeUnit.SECONDS)
        .build()
}
