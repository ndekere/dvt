package md.ke.dvt.data.network.retrofit

import md.ke.dvt.model.WeatherForecastResponse
import md.ke.dvt.model.WeatherResponse
import md.ke.dvt.model.places.PlaceDetailsResponse
import md.ke.dvt.model.places.PlacePhotoResponse
import md.ke.dvt.model.places.PlacesNearbyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitInterface {

    @GET("maps/api/place/nearbysearch/json?")
    suspend fun getNearbyPlaces(
        @Query("location") latlon: String,
        @Query("radius") radius: String,
        @Query("types") types: String,
        @Query("name") name: String,
        @Query("key") key: String
    ): Response<PlacesNearbyResponse>

    @GET("maps/api/place/textsearch/json?")
    suspend fun searchPlace(
        @Query("query") name: String,
        @Query("key") key: String
    ): Response<PlacesNearbyResponse>

    @GET("maps/api/place/details/json?")
    suspend fun placeDetails(
        @Query("placeid") placeId: String,
        @Query("key") key: String
    ): Response<PlaceDetailsResponse>

    @GET("maps/api/place/photos/json?")
    suspend fun placePhoto(
        @Query("place_reference") placePhotoReference: String,
        @Query("key") key: String
    ): Response<PlacePhotoResponse>

    @GET("data/2.5/weather?")
    suspend fun fetchCurrentWeatherByCity(
        @Query("q") city: String,
        @Query("appId") appId: String
    ): Response<WeatherResponse>

    @GET("data/2.5/weather?")
    suspend fun fetchCurrentWeather(
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("appId") appId: String
    ): Response<WeatherResponse>

    @GET("data/2.5/forecast?")
    suspend fun fetchWeatherForecastByCity(
        @Query("q") city: String,
        @Query("appId") appId: String
    ): Response<WeatherForecastResponse>

    @GET("data/2.5/forecast?")
    suspend fun fetchWeatherForecast(
        @Query("lat") latitude: String,
        @Query("lon") longitude: String,
        @Query("appId") appId: String
    ): Response<WeatherForecastResponse>
}
