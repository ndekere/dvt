package md.ke.dvt.data.network.retrofit

import androidx.databinding.ktx.BuildConfig
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import md.ke.dvt.DvtApplication
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInstance {
    private const val MAIN_URL = "https://api.openweathermap.org/"

    const val appId = "1304fcde9f2e9c1b0c1c81d999bdd919"

    var gson: Gson? = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        .create()

    private val client = buildOkhttpClient()

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(MAIN_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    val retrofitInterface: RetrofitInterface = getRetrofit().create(RetrofitInterface::class.java)
}
