package md.ke.dvt.model.places

data class ViewportXX(
    val northeast: NortheastXX,
    val southwest: SouthwestXX
)