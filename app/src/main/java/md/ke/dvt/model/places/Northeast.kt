package md.ke.dvt.model.places

data class Northeast(
    val lat: Double,
    val lng: Double
)