package md.ke.dvt.model.places

data class Geometry(
    val location: Location,
    val viewport: Viewport
)