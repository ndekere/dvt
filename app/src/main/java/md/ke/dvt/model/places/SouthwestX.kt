package md.ke.dvt.model.places

data class SouthwestX(
    val lat: Double,
    val lng: Double
)