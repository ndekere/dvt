package md.ke.dvt.model.places

data class OpeningHours(
    val open_now: Boolean
)