package md.ke.dvt.model.ext

data class Sys(
    val type: Int,
    val id: Int,
    val country: String,
    val sunrise: Long,
    val sunset: Long,
    val pod: String?
)
