package md.ke.dvt.model.places

data class ViewportX(
    val northeast: NortheastX,
    val southwest: SouthwestX
)