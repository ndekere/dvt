package md.ke.dvt.model.ext

data class Coord(
    val lon: Double,
    val lat: Double
)
