package md.ke.dvt.model.places

data class Location(
    val lat: Double,
    val lng: Double
)