package md.ke.dvt.model.places

data class LocationX(
    val lat: Double,
    val lng: Double
)