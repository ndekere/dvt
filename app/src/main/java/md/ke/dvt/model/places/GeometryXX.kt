package md.ke.dvt.model.places

data class GeometryXX(
    val location: LocationXX,
    val viewport: ViewportXX
)