package md.ke.dvt.model.places

data class Southwest(
    val lat: Double,
    val lng: Double
)