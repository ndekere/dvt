package md.ke.dvt.model.places

data class ResultXX(
    val address_components: List<AddressComponent>,
    val adr_address: String,
    val formatted_address: String,
    val geometry: GeometryXX,
    val icon: String,
    val icon_background_color: String,
    val icon_mask_base_uri: String,
    val name: String,
    val photos: List<PhotoXX>,
    val place_id: String,
    val reference: String,
    val types: List<String>,
    val url: String,
    val utc_offset: Int,
    val website: String
)