package md.ke.dvt.model.places

data class PlaceSearchResponse(
    val html_attributions: List<Any>,
    val results: List<ResultX>,
    val status: String
)