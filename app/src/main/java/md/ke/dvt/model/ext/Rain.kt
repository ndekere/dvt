package md.ke.dvt.model.ext

import com.google.gson.annotations.SerializedName

data class Rain(
    @SerializedName("3h")
    val threeh: Double
)
