package md.ke.dvt.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import md.ke.dvt.model.ext.City

@Entity(tableName = "WeatherForecastResponse")
data class WeatherForecastResponse(
    @PrimaryKey(autoGenerate = true) val local_id: Int,
    val cod: String,
    val message: Int,
    val cnt: Int,
    @SerializedName("list")
    val weatherForecast: ArrayList<WeatherForecast>,
    val city: City
)
