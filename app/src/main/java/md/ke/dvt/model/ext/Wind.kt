package md.ke.dvt.model.ext

data class Wind(
    val speed: Double,
    val deg: Int,
    val gust: Double?
)
