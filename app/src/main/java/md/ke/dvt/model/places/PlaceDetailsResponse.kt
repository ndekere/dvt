package md.ke.dvt.model.places

data class PlaceDetailsResponse(
    val html_attributions: List<Any>,
    val result: ResultXX,
    val status: String
)