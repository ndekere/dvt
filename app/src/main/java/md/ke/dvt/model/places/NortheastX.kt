package md.ke.dvt.model.places

data class NortheastX(
    val lat: Double,
    val lng: Double
)