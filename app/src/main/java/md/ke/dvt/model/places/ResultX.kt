package md.ke.dvt.model.places

data class ResultX(
    val formatted_address: String,
    val geometry: GeometryX,
    val icon: String,
    val icon_background_color: String,
    val icon_mask_base_uri: String,
    val name: String,
    val photos: List<PhotoX>,
    val place_id: String,
    val reference: String,
    val types: List<String>
)