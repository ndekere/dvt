package md.ke.dvt.model.places

data class LocationXX(
    val lat: Double,
    val lng: Double
)