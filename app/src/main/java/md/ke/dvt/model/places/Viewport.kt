package md.ke.dvt.model.places

data class Viewport(
    val northeast: Northeast,
    val southwest: Southwest
)