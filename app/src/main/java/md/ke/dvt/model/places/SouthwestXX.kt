package md.ke.dvt.model.places

data class SouthwestXX(
    val lat: Double,
    val lng: Double
)