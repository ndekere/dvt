package md.ke.dvt.model.places

data class PhotoXX(
    val height: Int,
    val html_attributions: List<String>,
    val photo_reference: String,
    val width: Int
)