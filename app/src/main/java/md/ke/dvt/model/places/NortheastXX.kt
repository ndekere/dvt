package md.ke.dvt.model.places

data class NortheastXX(
    val lat: Double,
    val lng: Double
)