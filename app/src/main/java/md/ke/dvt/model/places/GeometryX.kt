package md.ke.dvt.model.places

data class GeometryX(
    val location: LocationX,
    val viewport: ViewportX
)