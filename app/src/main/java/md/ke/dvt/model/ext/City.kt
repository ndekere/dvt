package md.ke.dvt.model.ext

data class City(
    val id: Int,
    val name: String,
    val coord: Coord,
    val country: String,
    val population: Int,
    val timezone: Long,
    val sunrise: Long,
    val sunset: Long
)
