package md.ke.dvt.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import md.ke.dvt.model.ext.*

@Entity(tableName = "WeatherResponse")
data class WeatherResponse(
    @PrimaryKey(autoGenerate = true) val local_id: Int?,
    val coord: Coord?,
    val weather: ArrayList<Weather>?,
    val base: String?,
    val main: Main?,
    val visibility: Int?,
    val wind: Wind?,
    val clouds: Clouds?,
    val dt: Long?,
    val sys: Sys?,
    val timezone: Int?,
    val id: Int?,
    val name: String?,
    val cod: Int?,
    var isFavourite: Int?
)
