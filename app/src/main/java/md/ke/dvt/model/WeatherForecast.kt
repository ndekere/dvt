package md.ke.dvt.model

import md.ke.dvt.model.ext.*

data class WeatherForecast(
    val dt: Long,
    val main: Main,
    val weather: ArrayList<Weather>,
    val clouds: Clouds,
    val wind: Wind,
    val visibility: Int,
    val pop: Double,
    val rain: Rain,
    val sys: Sys,
    val dt_txt: String,
)
