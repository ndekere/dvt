package md.ke.dvt.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favourite")
data class Favourite(
    @PrimaryKey(autoGenerate = true) val local_id: Int,
    val weatherResponse: WeatherResponse
)
