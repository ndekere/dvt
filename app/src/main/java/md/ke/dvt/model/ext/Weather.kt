package md.ke.dvt.model.ext

data class Weather(
    val id: Int,
    val main: String,
    val description: String?,
    val icon: String?
)
