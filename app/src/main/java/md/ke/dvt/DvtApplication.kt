package md.ke.dvt

import android.app.Application
import android.content.Context
import md.ke.dvt.data.db.DvtDatabase
import md.ke.dvt.data.network.retrofit.RetrofitInstance
import md.ke.dvt.data.repository.DvtRepository
import timber.log.Timber

class DvtApplication : Application() {

    val database by lazy { DvtDatabase.getInstance(this) }
    val repository by lazy { DvtRepository(RetrofitInstance.retrofitInterface, database) }

    private var appContext: Context? = null

    override fun onCreate() {
        super.onCreate()
        // initialize for any

        // Use ApplicationContext.
        appContext = applicationContext

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    fun getAppContext(): Context? {
        return appContext
    }
}
