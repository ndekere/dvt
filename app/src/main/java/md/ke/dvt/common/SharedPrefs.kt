package md.ke.dvt.common

import android.content.Context
import android.content.SharedPreferences

class SharedPrefs(context: Context) {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("shared_pref_dvt", Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = sharedPreferences.edit()

    fun setLat(lat: String) {
        editor.putString("dvt_lat", lat)
        editor.apply()
    }

    fun getLat(): String? {
        return sharedPreferences.getString("dvt_lat", null)
    }

    fun setLon(lon: String) {
        editor.putString("dvt_lon", lon)
        editor.apply()
    }

    fun getLon(): String? {
        return sharedPreferences.getString("dvt_lon", null)
    }
}
