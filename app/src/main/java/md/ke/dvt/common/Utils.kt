package md.ke.dvt.common

import android.content.Context
import android.location.LocationManager
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

private const val conveter: Double = 273.15

class Utils {

    fun convertKelvinToCelsius(double: Double) = roundOff(double.minus(conveter))

    private fun roundOffDecimal(number: Double): Double? {
        // To get two decimal places
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

    private fun roundOff(number: Double): String? {
        val df = DecimalFormat("#")
        return df.format(number).toString()
    }

    fun getDayFromEpochTime(timestamp: Long): String? {
        val calendar: Calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = timestamp * 1000L
        val date = Date(calendar.timeInMillis)
        val formatter: DateFormat = SimpleDateFormat("EEEE", Locale.ENGLISH)
        return formatter.format(date)
    }

    fun getDate(timestamp: Long): CharSequence? {
        val c = Calendar.getInstance()
        c.timeInMillis = (timestamp * 1000L)
        val d = c.time
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        return sdf.format(d)
    }

    fun getTime(timestamp: Long): CharSequence? {
        val c = Calendar.getInstance()
        c.timeInMillis = timestamp * 1000L
        val d = c.time
        val sdf = SimpleDateFormat("HH:mm")
        return sdf.format(d)
    }

    fun isLocationEnabled(context: Context): Boolean {
        var locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }
}
