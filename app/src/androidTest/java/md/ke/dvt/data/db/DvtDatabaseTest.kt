package md.ke.dvt.data.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.google.common.truth.Truth.assertThat
import com.google.gson.Gson
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import md.ke.dvt.data.db.dao.WeatherDao
import md.ke.dvt.model.WeatherResponse
import org.junit.After
import org.junit.Before
import org.junit.Test

class DvtDatabaseTest : TestCase() {

    private lateinit var database: DvtDatabase
    private lateinit var weatherDao: WeatherDao

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, DvtDatabase::class.java).build()
        weatherDao = database.weatherDao()
    }

    @Test
    fun insertAndFetchItem() = runBlocking {

        val resp = "{\n" +
            "    \"coord\": {\n" +
            "        \"lon\": 36.8333,\n" +
            "        \"lat\": -1.1667\n" +
            "    },\n" +
            "    \"weather\": [\n" +
            "        {\n" +
            "            \"id\": 800,\n" +
            "            \"main\": \"Clear\",\n" +
            "            \"description\": \"clear sky\",\n" +
            "            \"icon\": \"01n\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"base\": \"stations\",\n" +
            "    \"main\": {\n" +
            "        \"temp\": 292.92,\n" +
            "        \"feels_like\": 292.73,\n" +
            "        \"temp_min\": 291.86,\n" +
            "        \"temp_max\": 292.92,\n" +
            "        \"pressure\": 1021,\n" +
            "        \"humidity\": 68\n" +
            "    },\n" +
            "    \"visibility\": 10000,\n" +
            "    \"wind\": {\n" +
            "        \"speed\": 3.09,\n" +
            "        \"deg\": 50\n" +
            "    },\n" +
            "    \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "    },\n" +
            "    \"dt\": 1636914631,\n" +
            "    \"sys\": {\n" +
            "        \"type\": 1,\n" +
            "        \"id\": 2558,\n" +
            "        \"country\": \"KE\",\n" +
            "        \"sunrise\": 1636859530,\n" +
            "        \"sunset\": 1636903334\n" +
            "    },\n" +
            "    \"timezone\": 10800,\n" +
            "    \"id\": 192710,\n" +
            "    \"name\": \"Kiambu\",\n" +
            "    \"cod\": 200\n" +
            "}"

        val weatherResponse = Gson().fromJson(resp, WeatherResponse::class.java)

        weatherDao.insert(weatherResponse)

        val records = weatherDao.fetchCurrentWeatherLatest()

        assertThat(records == weatherResponse).isTrue()
    }

    @After
    public override fun tearDown() {
        database.close()
    }
}
