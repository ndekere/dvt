package md.ke.dvt.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import com.google.gson.Gson
import junit.framework.TestCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import md.ke.dvt.data.db.DvtDatabase
import md.ke.dvt.data.network.retrofit.RetrofitInstance
import md.ke.dvt.data.repository.DvtRepository
import md.ke.dvt.model.WeatherResponse
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class DvtViewModelTest : TestCase() {

    private lateinit var database: DvtDatabase
    private lateinit var repository: DvtRepository
    private lateinit var viewModel: DvtViewModel

    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)

    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, DvtDatabase::class.java).build()
        repository = DvtRepository(RetrofitInstance.retrofitInterface, database)
        viewModel = DvtViewModel(repository)
    }

    @Test
    fun insertAndFetchItem() = testDispatcher.runBlockingTest {

        val resp = "{\n" +
            "    \"coord\": {\n" +
            "        \"lon\": 36.8333,\n" +
            "        \"lat\": -1.1667\n" +
            "    },\n" +
            "    \"weather\": [\n" +
            "        {\n" +
            "            \"id\": 800,\n" +
            "            \"main\": \"Clear\",\n" +
            "            \"description\": \"clear sky\",\n" +
            "            \"icon\": \"01n\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"base\": \"stations\",\n" +
            "    \"main\": {\n" +
            "        \"temp\": 292.92,\n" +
            "        \"feels_like\": 292.73,\n" +
            "        \"temp_min\": 291.86,\n" +
            "        \"temp_max\": 292.92,\n" +
            "        \"pressure\": 1021,\n" +
            "        \"humidity\": 68\n" +
            "    },\n" +
            "    \"visibility\": 10000,\n" +
            "    \"wind\": {\n" +
            "        \"speed\": 3.09,\n" +
            "        \"deg\": 50\n" +
            "    },\n" +
            "    \"clouds\": {\n" +
            "        \"all\": 0\n" +
            "    },\n" +
            "    \"dt\": 1636914631,\n" +
            "    \"sys\": {\n" +
            "        \"type\": 1,\n" +
            "        \"id\": 2558,\n" +
            "        \"country\": \"KE\",\n" +
            "        \"sunrise\": 1636859530,\n" +
            "        \"sunset\": 1636903334\n" +
            "    },\n" +
            "    \"timezone\": 10800,\n" +
            "    \"id\": 192710,\n" +
            "    \"name\": \"Kiambu\",\n" +
            "    \"cod\": 200\n" +
            "}"

        val weatherResponse = Gson().fromJson(resp, WeatherResponse::class.java)

        testScope.launch(Dispatchers.IO) {
            // Insert into database.
            viewModel.addCurrentWeatherToRoom(weatherResponse)
            // Query database.
            val records = viewModel.getCurrentWeatherLocally().getOrAwaitValue()

            val recordName = records.name

            Truth.assertThat(recordName != null).isTrue()
        }
    }

    @After
    public override fun tearDown() {
        database.close()
    }
}
