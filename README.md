DVT Android Take Home Assignment
==============

Writing the DVT App using [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/), in 100% Kotlin, using Android Jetpack Components, and partly in Compose :rocket:

Requirements
----
- Create a weather application to display the current weather at the user’s location and a 5-day forecast:
  ![](docs/Forest_Sunny.png)

- The output for the current weather API looks like this:
 ```json
{
  "coord": {
    "lon": 36.8167,
    "lat": -1.2833
  },
  "weather": [
    {
      "id": 500,
      "main": "Rain",
      "description": "light rain",
      "icon": "10d"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 298.08,
    "feels_like": 297.86,
    "temp_min": 298.08,
    "temp_max": 298.77,
    "pressure": 1019,
    "humidity": 47
  },
  "visibility": 10000,
  "wind": {
    "speed": 6.17,
    "deg": 60
  },
  "rain": {
    "1h": 0.1
  },
  "clouds": {
    "all": 75
  },
  "dt": 1636976032,
  "sys": {
    "type": 1,
    "id": 2558,
    "country": "KE",
    "sunrise": 1636945933,
    "sunset": 1636989760
  },
  "timezone": 10800,
  "id": 184745,
  "name": "Nairobi",
  "cod": 200
}
 ```

- The output for the forecast weather API looks like this:
  [//]: # (I added the first and last list items, the response is 40 records long)

 ```json
 {
    "cod": "200",
    "message": 0,
    "cnt": 40,
    "list": [
        {
            "dt": 1636977600,
            "main": {
                "temp": 298.08,
                "feels_like": 297.86,
                "temp_min": 292.35,
                "temp_max": 298.08,
                "pressure": 1019,
                "sea_level": 1019,
                "grnd_level": 836,
                "humidity": 47,
                "temp_kf": 5.73
            },
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": {
                "all": 75
            },
            "wind": {
                "speed": 3.27,
                "deg": 45,
                "gust": 4.35
            },
            "visibility": 10000,
            "pop": 0.73,
            "rain": {
                "3h": 0.25
            },
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2021-11-15 12:00:00"
        },
        {
            "dt": 1637398800,
            "main": {
                "temp": 300.33,
                "feels_like": 299.99,
                "temp_min": 300.33,
                "temp_max": 300.33,
                "pressure": 1010,
                "sea_level": 1010,
                "grnd_level": 838,
                "humidity": 37,
                "temp_kf": 0
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "clouds": {
                "all": 26
            },
            "wind": {
                "speed": 4,
                "deg": 79,
                "gust": 2.83
            },
            "visibility": 10000,
            "pop": 0,
            "sys": {
                "pod": "d"
            },
            "dt_txt": "2021-11-20 09:00:00"
        }
    ],
    "city": {
        "id": 184745,
        "name": "Nairobi",
        "coord": {
            "lat": -1.2833,
            "lon": 36.8167
        },
        "country": "KE",
        "population": 2750547,
        "timezone": 10800,
        "sunrise": 1636945933,
        "sunset": 1636989760
    }
}
```

It is up to you to solve this problem using any technology/libraries you want to use. We will be assessing not just your ability to code, but your coding style as well. Please make sure to submit your best attempt at the problem – don’t be afraid to show off if you have the time!

### How it's built

* Technologies used
    * [Kotlin](https://kotlinlang.org/)
    * [Compose](https://developer.android.com/jetpack/compose) Because **COMPOSE IS FINALLY STABLE**
    * [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
    * [Flow](https://kotlinlang.org/docs/reference/coroutines/flow.html)
    * [Retrofit](https://square.github.io/retrofit/)
    * [Chucker](https://github.com/ChuckerTeam/chucker)
    * [Jetpack](https://developer.android.com/jetpack)
        * [Lifecycle](https://developer.android.com/topic/libraries/architecture/lifecycle)
        * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
    * [Timber](https://github.com/JakeWharton/timber)
    * [Leak Canary](https://github.com/square/leakcanary)

* Architecture
    * MVVM - Model View View Model

* Tests
    * [JUnit4](https://junit.org/junit4/)

* Plugins
    * [Dokka](https://github.com/Kotlin/dokka)
    * [jacoco](https://github.com/jacoco/jacoco)
    * [Ktlint](https://github.com/JLLeitschuh/ktlint-gradle)
    * [Detekt](https://github.com/detekt/detekt)

* CI/CD
    * [Gitlab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

### Screenshots

I added some screenshots in the `screenshots` folder, in the root directory of the project.

Current & Forecast Weather
---
![](screenshots/weather_dvt.jpeg)

Favorite Places
---
![](screenshots/favorite_dvt.jpeg)

View Place Details
---
![](screenshots/place_details_dvt.jpeg)

Favorite Places on Map
---
![](screenshots/place_on_map_dvt.jpeg)
